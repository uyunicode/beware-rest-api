/**
 * Import Dependencies
 */
import jwt from 'jsonwebtoken';
import { config as dotenv } from 'dotenv';
import { NextFunction, Request, Response } from 'express';

/**
 * Import Model User Account
 */
import { PoliceModel } from '../api/user_management/police_manage/model/police.model';

/**
 * @class json web token management class
 * and its verification of the accounts
 */
export class AuthJsonWebToken {

    /**
     * Attributes
     */
    public static policeModel: PoliceModel = new PoliceModel();

    /**
     * @public
     * process to verify the token every time a request is requested
     * @param req : request HTTP
     * @param res : response HTTP
     * @param next : continuation process to the function of a controller
     * @returns HTTP response or none, as it continued with the HTTP request
     */
    public static async verifyToken(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
        dotenv();
        try {
            let token: any = req.headers["x-access-token"];
            if (!token) {
                // not token received
                console.log('\x1b[32m', "no token received!!", '\x1b[0m');
                return res.status(403).json({ message: `Not token provided` });
            } else {
                // i have a token
                const decoded: any = jwt.verify(token, `${process.env.KEY_SECRET}`);
                // let userData = await this.policeModel.getDataUserByCode(decoded.code);
                // if (!userData) {
                if (!false) {
                    // if not user exists
                    return res.status(404).json({ message: `not user found` });
                } else {
                    // if user exists
                    console.log('\x1b[32m', "authenticated success", '\x1b[0m');
                }
            }
            console.log('\x1b[1m\x1b[0m', 'I am cyan');  //cyan
            console.log('\x1b[41m', 'sometext', '\x1b[0m');
            next();
        } catch (error) {
            console.log("Error is method verifyToken()", error);
            return res.status(500).json({ message: `Unauthorized` });
        }
    }

    /**
     * @public
     * process to verify the token of a possible victim user
     * @param token : token of possible victim
     * @returns : Array [status of verification token, ci possible victim]
     */
     public static verifyTokenPossibleVictim(token: string): Array<any> {
        dotenv();
        try {
            const decoded: any = jwt.verify(token, `${process.env.KEY_SECRET}`);
            return [true, decoded.code];
        } catch (error) {
            console.log("Error is method verifyToken()", error);
            return [false, null];
        }
    }

    /**
     * @public
     * Gets a token generated for a user who logged in 
     * and this token has a delay of 24 hours or 86400 seconds
     * @param accountUserCI : identifier code of the user who owns the token
     * @param typeAccount : type account (is victim possible or police )
     * @returns generated token
     */
    public static generateToken(accountUserCI: number, typeAccount: boolean): string {
        dotenv();
        return jwt.sign({ code: accountUserCI, type: typeAccount }, `${process.env.KEY_SECRET}`, { expiresIn: 86400 });
    }
}