/**
 * Import Dependencies
 */
 import * as AWS from "@aws-sdk/client-rekognition";

 /**
  * @class for use aws rekognition services
  */
 export class AwsRekognitionClient {
    private rekognitionClient: AWS.Rekognition = new AWS.Rekognition({ region: process.env.AWS_REGION }); 
    /**
      * @public
      * add faces into a colection
      * @param objectKey : filename of s3 object
      * @returns imageId assingned to faces detected
      */
     public addingFacesToACollection = async ( objectKey: string): Promise<string> => {
        let imageId: string = objectKey;
        var params = {
            CollectionId: "victimizer-collection", 
            DetectionAttributes: [
                "ALL", "DEFAULT"
            ], 
            ExternalImageId: imageId,  //The ID you want to assign to all the faces detected in the image.
            Image: {
             S3Object: {
              Bucket: process.env.AWS_BUCKET_NAME, 
              Name: `victimizer-profiles/${objectKey}`, 
             }
            }
        };  
        try {
            let data = await this.rekognitionClient.indexFaces(params);
            console.log(data);
            return imageId;
        } catch (error) {
            console.error("Error uploading file to s3: " + error);
            return "";
        }
     }

     public searchFacesByImage = async ( objectKeySource: string, collectionId: string): Promise<Array<any>> => {
        var params = {
            CollectionId: collectionId, 
            FaceMatchThreshold: 95, 
            Image: {
             S3Object: {
              Bucket: process.env.AWS_BUCKET_NAME, 
              Name: `request-help-files/${objectKeySource}`
             }
            }, 
            MaxFaces: 5,
            QualityFilter: "AUTO"
        };
        try {
            let data = await this.rekognitionClient.searchFacesByImage(params);
            console.log(data); // successful response
            let firstVictimizer = data.FaceMatches![0];
            return [firstVictimizer.Face?.ExternalImageId, firstVictimizer.Similarity];
        } catch (error) {
            console.error("Error uploading file to s3: " + error);
            return [];
        }
     }
 }