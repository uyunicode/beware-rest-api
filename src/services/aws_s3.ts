/**
 * Import Dependencies
 */
 import {v4 as uuidV4} from 'uuid';
 import { S3Client,PutObjectCommand } from "@aws-sdk/client-s3";
 /**
  * @class for upload objects to aws s3 services
  */
 export class AwsS3Client {
    private s3Client: S3Client = new S3Client({ region: process.env.AWS_REGION }); 
    /**
      * @public
      * Send a object file to s3 bucket
      * @param file : file to upload
      * @returns filename to object
      */
     public sendObject = async (file: Buffer| undefined, path: string): Promise<string> => {
        let filename: string = `${uuidV4()}.jpeg`;
        const params = {
            Bucket: process.env.AWS_BUCKET_NAME, // The name of the bucket. For example, 'sample_bucket_101'.
            Key: `${path}${filename}`, // The name of the object. For example, 'sample_upload.txt'.
            Body: file, // The content of the object. For example, 'Hello world!".
        };
        try {
            const results = await this.s3Client.send(new PutObjectCommand(params));
            console.log("Successfully created " + params.Key + 
                        " and uploaded it to " + params.Bucket + "/" + params.Key );
            return filename;
        } catch (error) {
            console.error("Error uploading file to s3: " + error);
            return "";
        }
     }
 }