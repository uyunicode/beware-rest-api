// Download the helper library from https://www.twilio.com/docs/node/install
// Find your Account SID and Auth Token at twilio.com/console
// and set the environment variables. See http://twil.io/secure
import twilio, { Twilio } from 'twilio';

/**
 * @class Twilio Services
 * send notification message
 */
export class TwilioService {

    private static accountSid: string | undefined = process.env.TWILIO_ACCOUNT_SID;
    private static authToken: string | undefined = process.env.TWILIO_AUTH_TOKEN;
    private static numberPhone: string | undefined = process.env.TWILIO_PHONE_NUMBER;
    private static client: Twilio = twilio(TwilioService.accountSid, TwilioService.authToken);

    /**
     * send SMS message
     * @param numberPhone : phone number
     */
    public static sendMessage = async (numberPhone: string, nameVictim: string) => {
        try {
            let responseMessage = await TwilioService.client.messages.create({
                body: `Mensaje de auxilio!! Tu amiga(o) ${nameVictim} de confianza pidio ayuda. contactate con ella(el) inmediatamente`,
                from: TwilioService.numberPhone || '+16674018357',
                to: numberPhone
            });
            console.log(responseMessage);
        } catch (error) {
            console.log("Error in send message with twilio service: ", error);
        }
    }

    /**
     * call number phone
     * @param numberPhone :phone number
     */
    public static callPhone = async (numberPhone: string) => {
        let responseCall = await TwilioService.client.calls.create({
            url: 'http://demo.twilio.com/docs/voice.xml',
            to: numberPhone,
            from: TwilioService.numberPhone || '+16674018357'
        });
        console.log(responseCall.sid);
    }

    /**
     *! IN MAINTINANCE.
     * send message whatsapp to number phone
     * @param numberPhone : number phone
     */
    public static sendWhatsAppMessage = async (numberPhone: string) => {
        console.log("initialize sendWhatsAppMessage");
        let responseWhatsAppMessage = await TwilioService.client.messages.create({
            from: `whatsapp:${TwilioService.numberPhone || '+16674018357'}`,
            body: 'Hola te saluda Beware software desde tu rest api',
            to: `whatsapp:${numberPhone}`
        });
        console.log(responseWhatsAppMessage);
    }
}