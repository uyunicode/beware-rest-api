import nodemailer, { Transporter } from 'nodemailer';


export class Mail {
    /**
     * send email to client
     * @param to : destiny email
     * @param subject : subject email
     * @param body : contains email
     * @returns : true or false
     */
    public static sendMail = async (to: string, subject: string, body: string): Promise<boolean> => {
        try {
            let transporter: Transporter = await nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: process.env.MAIL_USERNAME,
                    pass: process.env.MAIL_PASSWORD
                },
                tls: { rejectUnauthorized: false }
            });
            transporter.verify(function (error, success) {
                if (error) {
                    console.log("Error connection to email server and account", error);
                } else {
                    console.log("Server is ready to take our messages: ", success);
                }
            });
            const info = await transporter.sendMail({
                from: "'Beware Software' <beware_server@beware.net>",
                to,
                subject,
                html: body
            });
            console.log("info the email: ", info);
            return info;
        } catch (error) {
            console.log("Error in send email ", error);
            return false;
        }
    }
}