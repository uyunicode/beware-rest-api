/**
 * Import dependencies
 */
import express, { Application } from 'express';
import morgan from 'morgan';
import cors from 'cors';
import swaggerUI from 'swagger-ui-express';
import swaggerJsDoc from 'swagger-jsdoc';
import path from 'path';

/**
 * Import settings files
 */
import { options } from './swaggerOptions';
import { AuthJsonWebToken } from './middlewares/auth';

/**
 * Import Routers
 */
import policeUserRouter from './api/user_management/police_manage/routes/police_user.routes';
import possibleVictimRouter from './api/user_management/possible_victim_manage/routes/possible_victim.routes';
import trustedPersonRouter from './api/user_management/trusted_person_manage/routes/trusted_person.routes';
import trustedContactRouter from './api/user_management/trusted_contact_manage/routes/trusted_contact.routes';
import contactRouter from './api/user_management/contact_manage/routes/contact.routes';
import requestHelpRouter from './api/request_help_management/request_help_manage/routes/request_help.routes';
import victimizerRouter from './api/user_management/victimizer_manage/routes/victimizer.routes';
import intrafamilyViolenceTestRouter from './api/request_help_management/intrafamily_violence_test_manage/routes/intrafamily_violence_test.routes';


/**
 * @class App for the initialization of the web server
 * and its settings and middlewares
 */
export class App {

    /**
     * Attributes
     */
    private app: Application;

    /**
     * Constructor that initializes express, middelwares and routes
     * @constructor 
     * @param port port for initialize server
     */
    constructor(port?: number | string) {
        this.app = express();
        this.app.set('PORT', process.env.PORT || port || 3000);
        this.middlewares();
        this.routes();
    }

    /**
     * @private
     * Run these processes before every Request
     * @returns nothing, is a void
     */
    private middlewares(): void {
        const publicPath = path.resolve(__dirname, 'public');
        this.app.use(express.static(publicPath));
        this.app.use(morgan('dev'));
        this.app.use(cors());
        this.app.use(express.json());
    }

    /**
     * @private
     * Method for starting routes for HTPP services
     * @returns nothing, is a void
     */
    private routes(): void {
        // swagger ui server
        let specs = swaggerJsDoc(options);
        /**
         * @swagger
         * /docs :
         *  get:
         *      summary: Returns the web server documentation
         *      responses:
         *          200:
         *              description: Obtain the documentation of the enabled routes
         *              content:
         *                  html:
         *                      schema:
         *                          type: html
         *                          description: doc swagger
         */
        this.app.use('/docs', swaggerUI.serve, swaggerUI.setup(specs));
        // listen routers
        this.app.use('/user_management/police_manage', policeUserRouter);
        this.app.use('/user_management/possible_victim_manage', possibleVictimRouter);
        this.app.use('/user_management/trusted_person_manage', trustedPersonRouter);
        this.app.use('/user_management/trusted_person_manage', trustedContactRouter);
        this.app.use('/user_management/contact_manage', contactRouter);
        this.app.use('/request_help_management/request_help_manage', requestHelpRouter);
        this.app.use('/user_management/victimizer_manage', victimizerRouter);
        this.app.use('/request_help_management/intrafamily_violence_test_manage', intrafamilyViolenceTestRouter);
    }

    /**
     * @public
     * Asynchronous process for port initialization with the web server
     * @returns nothing, is a void
     */
    public async listen(): Promise<void> {
        let server = await require('http').createServer(this.app);
        let io = await require('socket.io')(server);

        io.on("connection", (client: any) => {
            console.log("client connected");
            let token: string = client.handshake.headers['x-access-token']; //for flutter client
            if(typeof token == "undefined"){
                token = client.handshake.query.token; //for react client
            }
            const [validate, ci] = AuthJsonWebToken.verifyTokenPossibleVictim(token);
            console.log(`Ci ${ci}, validate: ${validate}`);
            if (!validate) {
                return client.disconnect();
            }
            client.on('disconnect', () => {
                console.log("client disconnected");
            });
            client.join(ci);
            client.on('position', (payload: any) => {
                console.log(payload);
                for (let index = 0; index < payload.to.length; index++) {
                    const ciPersonTrusted = payload.to[index];
                    io.to(ciPersonTrusted).emit('position', payload);
                }
            });
            client.on('notification', (payload: any) => {
                console.log(payload);
                for (let index = 0; index < payload.to.length; index++) {
                    const ciPersonTrusted = payload.to[index];
                    io.to(ciPersonTrusted).emit('notification', payload.data);
                }
            });
        });
        await server.listen(this.app.get('PORT'));
        console.log("\x1b[46m", "\x1b[30m", `Server on port ${this.app.get('PORT')}`, "\x1b[0m");
    }
}