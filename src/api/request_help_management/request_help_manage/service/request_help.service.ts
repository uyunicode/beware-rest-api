/**
 * Import Components 
 */

/**
 * Import Models
 */
import { RequestHelp } from "../model/request_help";
import { RequestHelpModel } from "../model/request_help.model";
import { ContactModel } from "../../../user_management/contact_manage/model/contact.model";
import { Contact } from "../../../user_management/contact_manage/model/contact";
import { TwilioService } from "../../../../services/twilio";
import { PossibleVictimModel } from "../../../user_management/possible_victim_manage/model/possibleVictim.model";
import { AwsS3Client } from '../../../../services/aws_s3';
import { AwsRekognitionClient } from '../../../../services/aws_rekognition';
import {VictimizerModel} from '../../../user_management/victimizer_manage/model/victimizer.model';
import { Victimizer } from "api/user_management/victimizer_manage/model/victimizer";

/**
 * @class of business logic implementation for possible victim manage services users.
 */
export class RequestHelpService {

    /**
     * Attributes
     */
    private requestHelpModel: RequestHelpModel;
    private contactTrustedModel: ContactModel;
    private possibleVictim: PossibleVictimModel;
    private victimizer: VictimizerModel;

    /**
     * @public
     * @constructor
     * initializes the instances of the models dependent on this service.
     */
    public constructor() {
        this.requestHelpModel = new RequestHelpModel();
        this.contactTrustedModel = new ContactModel();
        this.possibleVictim = new PossibleVictimModel();
        this.victimizer = new VictimizerModel();
    }

    /**
     * @public
     * Register new request help
     * @param requestHelp : request help data to save
     * @returns json response with status and message if success save
     */
    public createRequestHelp = async (requestHelp: RequestHelp): Promise<any> => {
        try {
            let code: number = await this.requestHelpModel.create(requestHelp);
            if (code > 0) {
                let listContacts: Array<Contact> = await this.contactTrustedModel.getListContactsOfPossibleVictim(requestHelp.ci_victim);
                let possibleVictimData = await this.possibleVictim.findByCI(requestHelp.ci_victim);
                await listContacts.forEach(async (contact: Contact) => {
                    console.log(contact);
                    await TwilioService.sendMessage(contact.cellphone, possibleVictimData ? possibleVictimData.name : "Sin Nombre");
                    await TwilioService.callPhone(contact.cellphone);
                });
                return {
                    status: 200,
                    msg: "created requets help successfully",
                    data: code
                };
            } else {
                return {
                    status: 500,
                    msg: "error in create request help"
                };
            }
        } catch (error) {
            console.log("Error in request help service createRequestHelp()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * @public
     * @param code : request help code
     * @param numberFile : number file to update
     * @param file : file to upload
     * @returns json response with status, message and data
     */

    public saveImageRequestHelp = async (code: number, numberFile: number, file: Buffer| undefined): Promise<any> => {
        try {
            let awsClient: AwsS3Client = new AwsS3Client();
            let filename: string = await awsClient.sendObject(file, "request-help-files/");
            if(await this.requestHelpModel.updateFileName(code, filename, numberFile))
                return {status: 200, message: "saveImageRequestHelp successfully",  data: filename};
            return {status: 500, msg: "error in saveImageRequestHelp"};
        } catch (error) {
            console.log("Error in request help service saveImageRequestHelp()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * get list request help
     * @param ciPossibleVictim : ci possible victim mobile device
     * @returns json response
     */
    public getListRequestHelpOfPossibleVictim = async (ciPossibleVictim: number): Promise<any> => {
        try {
            let listRequestHelpOfVictim = await this.requestHelpModel.getListRequestHelpOfPossibleVictim(ciPossibleVictim);
            return {
                status: 200,
                msg: "your request help list",
                data: listRequestHelpOfVictim
            }
        } catch (error) {
            console.log("Error in ge tList Request Help Of Possible Victim", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * @public
     * get all list of request help
     * @returns json response
     */
    public getRequestHelpList = async (): Promise<any> => {
        try {
            let requestHelpList = await this.requestHelpModel.findAll();
            return {
                status: 200,
                msg: "request help list",
                data: requestHelpList
            }
        } catch (error) {
            console.log("Error in getRequestList() ", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * @public
     * get request help by code
     * @param code : number of request help
     * @returns json response
     */
     public getRequestHelp = async (code: number): Promise<any> => {
        try {
            let requestHelp = await this.requestHelpModel.findByCode(code);
            return {
                status: 200,
                msg: "request help by code",
                data: requestHelp
            }
        } catch (error) {
            console.log("Error in getRequest ", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * @public
     * @param code : request help code
     * @param filename2 : file 1 to compare
     * @param filename1 : file 1 to compare
     * @returns json response with status, message and data
     */

     public updateCiVictimizerOfRequestHelp = async (code: number, filename1: string, filename2: string): Promise<any> => {
        try {
            let awsRekognitionClient: AwsRekognitionClient = new AwsRekognitionClient();
            let victimizer:Victimizer| undefined;
            let searchImage:Array<any>;
            let similarity: number = 0.00;
            if(filename1!="a.jpg"){
                searchImage = await awsRekognitionClient.searchFacesByImage(filename1, "victimizer-collection");
                if(searchImage.length>0) { //found a victimizer
                    victimizer = await this.victimizer.findByProfile(searchImage[0]);
                    similarity = searchImage[1];
                }else if(filename2!="b.jpg"){ //not found; search with filename2
                    searchImage = await awsRekognitionClient.searchFacesByImage(filename2, "victimizer-collection");
                    if(searchImage.length>0){
                        victimizer = await this.victimizer.findByProfile(searchImage[0]);
                        similarity = searchImage[1];
                    }
                }
            }
            if(await this.requestHelpModel.updateCiVictimizer(code, victimizer!.ci))
                return {status: 200, message: "update ci victimizer successfully",  data: {victimizer, similarity }};
            return {status: 500, msg: "error in updateCiVictimizerOfRequestHelp"};
        } catch (error) {
            console.log("Error in request help service updateCiVictimizerOfRequestHelp", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

}