/**
 * Import Dependencies
 */
import { Request, Response } from "express";
import { RequestHelpService } from "../service/request_help.service";

/**
 * Import Services
 */

/**
 * @class controller possible victim user, for the implementation of HTTP requests
 */
export class RequestHelpController {

    /**
     * Attributes
     */
    private requestHelpService: RequestHelpService;

    /**
     * @constructor
     * Initialize possible victim user service for implement bussiness logic
     */
    public constructor() {
        this.requestHelpService = new RequestHelpService();
    }

    /**
     * @public
     * create a new request help
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns json to client response
     */

    public createRequestHelp = async (request: Request, response: Response) => {
        try {
            let { date_time, initial_location, filename1, filename2, ci_victim } = request.body;
            let ci_victimizer = 0; //victimizer default value
            let result = await this.requestHelpService.createRequestHelp({ code: 0, date_time, initial_location, filename1, filename2, ci_victim, ci_victimizer });
            return response.status(result.status).json(result);
        } catch (error) {
            console.log("Error in createRequestHelp controller", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }

    /**
     * @public
     * save image into s3 bucket
     * @param request : HTTP request
     * @param response : HTTP response
     * @returns json to client response
     */
    public saveImageRequestHelp = async(request: Request, response: Response) => {
        try {
            
            let {code, numberFile} = request.body;
            let file: Buffer| undefined= request.file?.buffer;
            let result = await this.requestHelpService.saveImageRequestHelp(code, numberFile, file);
            return response.status(result.status).json(result);
        } catch (error) {
            console.log("Error in saveImageRequestHelp controller", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }

    public getListRequestHelpOfPossibleVictim = async (request: Request, response: Response): Promise<Response> => {
        let { ci_victim } = request.params;
        let result = await this.requestHelpService.getListRequestHelpOfPossibleVictim(parseInt(ci_victim));
        return response.status(result.status).json(result);
    }

    /**
     * @public
     * get all request help list
     * @param request : HTTP request
     * @param response : HTTP response
     * @returns json with status, message and data
     */
    public getRequestHelpList = async (request: Request, response: Response): Promise<Response> => {
        try {
            let result = await this.requestHelpService.getRequestHelpList();
            return response.status(result.status).json(result);
        } catch (error) {
            console.log("Error in getRequestHelpList controller", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }

    /**
     * @public
     * get request help by id
     * @param request : HTTP request
     * @param response : HTTP response
     * @returns json with status, message and data
     */
     public getRequestHelp = async (request: Request, response: Response): Promise<Response> => {
        try {
            let {code} = request.params;
            let result = await this.requestHelpService.getRequestHelp(parseInt(code));
            return response.status(result.status).json(result);
        } catch (error) {
            console.log("Error in getRequestHelp controller", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }

    /**
     * @public
     * search by image and update ci victimizer of a request help
     * @param request : HTTP request
     * @param response : HTTP response
     * @returns json to client response
     */
     public updateCiVictimizerOfRequestHelp = async(request: Request, response: Response) => {
        try {
            let {code} = request.params;
            let {filename1, filename2} = request.body;
            console.log(`f1: ${filename1}, f2: ${filename2}`);
            let result = await this.requestHelpService.updateCiVictimizerOfRequestHelp(parseInt(code), filename1, filename2);
            return response.status(result.status).json(result);
        } catch (error) {
            console.log("Error in updateCiVictimizerOfRequestHelp controller", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }
}