/**
 * Import Libraries
 */
import { Router } from 'express';
import multer from 'multer';
/**
 * Import Files
 */

/**
 * Import Controllers
 */
import { RequestHelpController } from '../controller/request_help.controller';

// initialize Router
const routes = Router();
// initialize multer
var upload = multer({storage: multer.memoryStorage()});
// initialize controllers
const requestHelpController: RequestHelpController = new RequestHelpController();

/**
 * @swagger
 * /request_help_management/request_help_manage/create_request_help :
 *  post:
 *      summary: create request help
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: a json with a message and a status
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.post('/create_request_help', requestHelpController.createRequestHelp);

/**
 * @swagger
 * /request_help_management/request_help_manage/save_image_request_help :
 *  post:
 *      summary: save images of a request help
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: a json with a message and a status
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              data:
 *                                  type: number
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
 routes.post('/save_image_request_help',upload.single('image'), requestHelpController.saveImageRequestHelp);

/**
 * @swagger
 * /request_help_management/request_help_manage/get_request_help_list/:ci_victim :
 *  get:
 *      summary: get request help list of a possible victim
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: returns json success if ci_victim exists but if not saved send http status 500 or 400
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              data:
 *                                  type: array
 *                                  description: list request help
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.get('/get_request_help_list/:ci_victim', requestHelpController.getListRequestHelpOfPossibleVictim);

/**
 * @swagger
 * /request_help_management/request_help_manage/request_help_list :
 *  get:
 *      summary: get request help list
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: returns a json with request help list
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              data:
 *                                  type: array
 *                                  description: list request help
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
 routes.get('/request_help_list', requestHelpController.getRequestHelpList);

 /**
 * @swagger
 * /request_help_management/request_help_manage/:code :
 *  get:
 *      summary: get request help by code
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: returns a json with request help by code
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              data:
 *                                  type: object
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
  routes.get('/:code', requestHelpController.getRequestHelp);

/**
 * @swagger
 * /request_help_management/request_help_manage/update_ci_victimizer/:code :
 *  get:
 *      summary: update ci victimizer of a request help
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: returns json success if ci_victim exists but if not saved send http status 500 or 400
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              data:
 *                                  type: array
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.put('/update_ci_victimizer/:code', requestHelpController.updateCiVictimizerOfRequestHelp);

export default routes;