export interface RequestHelp {
    code: number,
    date_time: string,
    initial_location: string,
    filename1: string,
    filename2: string,
    ci_victim: number,
    ci_victimizer: number
}