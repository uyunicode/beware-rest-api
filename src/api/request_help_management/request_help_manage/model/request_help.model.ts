/**
 * Import Dependencies
 */
import { request } from "express";
import { QueryResult } from "pg";
/**
 * Import Components
 */
import { Connection } from "../../../../config/database/connection";
import { RequestHelp } from "./request_help";


/**
 * @class Model for save database.
 */
export class RequestHelpModel {

    /**
     * Attributes
     */
    public connection: Connection;

    /**
     * @public
     * @constructor
     * Obtain an instance of connection to database
     */
    public constructor() {
        // obtiene la instancia a traves de singleton
        this.connection = Connection.getInstance();
    }
    /**
     * @public
     * create a new request help
     * @param requestHelp : new data request help
     * @returns requestHelp code if saved succes else -1 if has a problem to saved or query invalidated
     */
    public create = async (requestHelp: RequestHelp): Promise<number> => {
        try {
            let code: QueryResult = await this.connection.executeQuerySQL(`insert into help_request (date_time, initial_location, filename1, filename2, ci_victim, ci_victimizer) values('${requestHelp.date_time}', '${requestHelp.initial_location}', '${requestHelp.filename1}', '${requestHelp.filename2}', ${requestHelp.ci_victim}, ${requestHelp.ci_victimizer}) returning code;`);
            return code.rows[0].code;
         } catch (error) {
            console.log("Error in create Possible Victim Model", error);
            return -1;
         }
    }

    /**
     * @public
     * update filename of request help
     * @param code : request help code
     * @param filename : name of file to update
     * @param numberFile: number file to update
     * @returns true if update succes else false if has a problem to update or query invalidated
     */
     public updateFileName = async (code: number, filename: string, numberFile: number): Promise<boolean> => {
        try {
            if (numberFile==1) {
                await this.connection.executeQuerySQL(`update help_request set filename1 = '${filename}' where code = ${code};`);   
            }else{
                await this.connection.executeQuerySQL(`update help_request set filename2 = '${filename}' where code = ${code};`);
            }
            return true;
         } catch (error) {
            console.log("Error in create Possible Victim Model", error);
            return false;
         }
    }

    /**
     * get list request helpers of possible victim
     * @param ciPossibleVictim : ci possible victim
     * @returns list request
     */
    public getListRequestHelpOfPossibleVictim = async (ciPossibleVictim: number): Promise<Array<RequestHelp>> => {
        try {
            let listRequestHelp: QueryResult = (await this.connection.executeQuerySQL(
                `select code, to_char(date_time,'Mon DD-YYYY HH:MI Day') as date_request, initial_location, filename1, filename2, ci_victim, u."name"
                from help_request hr, "user" u where hr.ci_victim = u.ci and ci_victim in (
                                            select ci_person_trusted from trusted_person tp where ci_victim = ${ciPossibleVictim});`));
            return listRequestHelp.rows;
        } catch (error) {
            console.log("Error in method getListContactsOfPossibleVictim: " + error);
            return [];
        }
    }
    
    /**
     * @public
     * get all request help list
     * @returns List of request help
     */
    public findAll = async (): Promise<Array<RequestHelp>> => {
        try {
            let listRequestHelp: QueryResult = (await this.connection.executeQuerySQL(
                `select code,  date_time, initial_location , filename1 , filename2 , ci_victim, u."name", u.cellphone, ci_victimizer
                from help_request hp, "user" u 
                where hp.ci_victim = u.ci and u.type_user = true
                order by date_time desc;`));
            return listRequestHelp.rows;
        } catch (error) {
            console.log("Error in method getList " + error);
            return [];
        }
    }

    /**
     * @public
     * get a object with request help data
     * @param code : number of request help
     * @returns the request help of code provided or undefined
     */
    public findByCode = async (code: number): Promise<RequestHelp| undefined> =>{
        try {
            let requestHelp: QueryResult = (await this.connection.executeQuerySQL(
                `select code,  date_time, initial_location , filename1 , filename2 , ci_victim, u."name", u.cellphone, ci_victimizer, v."name" as victimizer_name, v.profile 
                from help_request hp, "user" u , victimizer v
                where hp.ci_victim = u.ci and hp.ci_victimizer = v.ci and u.type_user = true and hp.code = ${code};`));
            return requestHelp.rows[0];
        } catch (error) {
            console.log("Error in method findByCode: " + error);
            return undefined;
        }
    }

    /**
     * @public
     * update ci_victimzer of a request help
     * @param code : request help code
     * @param ci_victimzer : ci of victimizer
     * @returns true if update succes else false if has a problem to update or query invalidated
     */
     public updateCiVictimizer = async (code: number, ci_victimizer: number): Promise<boolean> => {
        try {
            await this.connection.executeQuerySQL(`update help_request set ci_victimizer = ${ci_victimizer} where code = ${code};`);   
            return true;
         } catch (error) {
            console.log("Error in update Model", error);
            return false;
         }
    }

}