/**
 * Import Dependencies
 */
import { Request, Response } from "express";

/**
 * Import Services
 */
import { IntraFamilyViolenceTestService } from '../service/intrafamily_violence_test.service';

/**
* @class controller victimizer, for the implementation of HTTP requests
*/
export class IntraFamilyViolenceTestController {
    /**
     * Attributes
     */
    private intrafamilyViolenceTestService: IntraFamilyViolenceTestService;

    /**
     * @constructor
     * Initialize possible victim user service for implement bussiness logic
     */
    public constructor() {
        this.intrafamilyViolenceTestService = new IntraFamilyViolenceTestService();
    }

    /**
    * get a list with questions of test
    * @param request : request HTTP
    * @param response : response HTTP
    * @returns a json with estate http, message and data
    */
    public getQuestionsList = (request: Request, response: Response): Response => {
        let questionList = this.intrafamilyViolenceTestService.getQuestionsList();
        return response.status(questionList.status).json(questionList);
    }

    /**
     * test answers of questions 
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns a json with estate http, message and data
     */
    public testAnswersViolenceTest = (request: Request, response: Response): Response => {
        let answersList: Array<number> = request.body.answers;
        console.table({ answersList });
        let anwserTest = this.intrafamilyViolenceTestService.testAnswersViolenceTest(answersList);
        return response.status(anwserTest.status).json((anwserTest));
    }
}