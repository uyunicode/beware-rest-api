/**
 * Import Components 
 */

import { questions } from "../model/intrafamily_violence_test";
import { QUANTITY_QUESTIONS_PHASE1, QUANTITY_QUESTIONS_PHASE2, QUANTITY_QUESTIONS_PHASE3, QUANTITY_OPTIONS } from '../service/quantity_questions_phase';

/**
* @class of business logic implementation for intrafamily violence test
*/
export class IntraFamilyViolenceTestService {
    /**
     * Attributes
     */
    private questionsList: Array<string>;

    /**
     * @public
     * @constructor
     * initializes the instances of the models dependent on this service.
     */
    public constructor() {
        this.questionsList = questions;
    }

    /**
     * @public
     * Get list of questions
     * @returns list questions
    */
    public getQuestionsList = (): any => {
        return {
            status: 200,
            msg: "get list questions",
            data: this.questionsList
        };
    }

    /**
     * @public
     * get the phase it is in
     * @param answerList : list of answer
     * @returns the phase it is in
     * @test
        example test
        {
            "answers": [ 1,2,1,1,1,1,1,1,1,1,2,1,2,3,2,1,2,3,1,1,1,2,1,1,1,1,1,1,1,2,1,1,2 ]
        }
     */
    public testAnswersViolenceTest = (answerList: Array<number>): any => {
        // first phase test
        let percentageSumPhase1: number = this.getPercentageSumPhase(
            answerList,
            0,
            QUANTITY_QUESTIONS_PHASE1,
            (100 / QUANTITY_QUESTIONS_PHASE1) / QUANTITY_OPTIONS);
        // second phase test
        let percentageSumPhase2: number = this.getPercentageSumPhase(
            answerList,
            QUANTITY_QUESTIONS_PHASE1,
            QUANTITY_QUESTIONS_PHASE1 + QUANTITY_QUESTIONS_PHASE2,
            (100 / QUANTITY_QUESTIONS_PHASE2) / QUANTITY_OPTIONS);
        // third phase test
        let percentageSumPhase3: number = this.getPercentageSumPhase(
            answerList,
            QUANTITY_QUESTIONS_PHASE1 + QUANTITY_QUESTIONS_PHASE2,
            QUANTITY_QUESTIONS_PHASE1 + QUANTITY_QUESTIONS_PHASE2 + QUANTITY_QUESTIONS_PHASE3,
            (100 / QUANTITY_QUESTIONS_PHASE3) / QUANTITY_OPTIONS);

        console.table({
            percentageSumPhase1,
            percentageSumPhase2,
            percentageSumPhase3
        });
        // PRINTED #1
        // │ percentageSumPhase1 │  20  │
        // │ percentageSumPhase2 │  30  │
        // │ percentageSumPhase3 │  50  │
        //          ---------------------
        //                          100%
        percentageSumPhase1 = percentageSumPhase1 * 0.20;
        percentageSumPhase2 = percentageSumPhase2 * 0.30;
        percentageSumPhase3 = percentageSumPhase3 * 0.50;
        // get total accumulate
        let totalPercentage = percentageSumPhase1 + percentageSumPhase2 + percentageSumPhase3;
        console.table({
            percentageSumPhase1,
            percentageSumPhase2,
            percentageSumPhase3
        });
        console.log(totalPercentage);
        // PRINTED #2
        // │ percentageSumPhase1 │         70         │
        // │ percentageSumPhase2 │ 124.24242424242425 │
        // │ percentageSumPhase3 │ 141.66666666666669 │
        let numberPhase = 3;
        if (totalPercentage == 0)
            numberPhase = 0;
        else if (totalPercentage > 0 && totalPercentage < 20)
            numberPhase = 1;
        else if (totalPercentage >= 20 && totalPercentage < 49)
            numberPhase = 2;
        else if (totalPercentage >= 49)
            numberPhase = 3;
        return {
            status: 200,
            msg: "result of test",
            data: numberPhase
        };
    }

    private getPercentageSumPhase = (answerList: Array<number>, start: number, end: number, percentageValueQuestion: number): number => {
        let percentageSumPhase: number = 0.00;
        for (let i = start; i < end; i++) {
            const answer = answerList[i];
            if (answer == 1) percentageSumPhase = percentageSumPhase + 0;
            if (answer == 2) percentageSumPhase = percentageSumPhase + 2 * percentageValueQuestion;
            if (answer == 3) percentageSumPhase = percentageSumPhase + 3 * percentageValueQuestion;
        }
        return percentageSumPhase;
    }


}