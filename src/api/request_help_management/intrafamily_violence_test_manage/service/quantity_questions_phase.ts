const QUANTITY_QUESTIONS_PHASE1: number = 10;  //number of questions phase 1
const QUANTITY_QUESTIONS_PHASE2: number = 11;  //number of questions phase 2
const QUANTITY_QUESTIONS_PHASE3: number = 12;  //number of questions phase 3
const QUANTITY_OPTIONS = 3;  //number of options (never, sometimes, usually)

export {QUANTITY_QUESTIONS_PHASE1, QUANTITY_QUESTIONS_PHASE2, QUANTITY_QUESTIONS_PHASE3, QUANTITY_OPTIONS};