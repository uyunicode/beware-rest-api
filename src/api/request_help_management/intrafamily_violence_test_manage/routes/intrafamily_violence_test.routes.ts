/**
 * Import Libraries
 */
import { Router } from 'express';

/**
* Import Controllers
*/
import { IntraFamilyViolenceTestController } from '../controller/intrafamily_violence_test.controller';

// initialize Router
const routes = Router();

// initialize controllers
let intrafamilyViolenceTestController = new IntraFamilyViolenceTestController();

/**
 * @swagger
 *  /request_help_management/intrafamily_violence_test_manage/get_questions_test :
 *  get:
 *      summary: Get questions list
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: obtained successfully
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              data:
 *                                  type: object  
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.get('/get_questions_test', intrafamilyViolenceTestController.getQuestionsList);

/**
* @swagger
*  /request_help_management/intrafamily_violence_test_manage/test_answers :
*  get:
*      summary: evaluate answers test
*      requestBody:
*              content:
*                  application/json:
*                       answers:
*                           type: array
*      responses:
*          200:
*              description: result test intrafamily violence
*              content:
*                  application/json:
*                      schema:
*                          type: object
*                          properties:
*                              state:
*                                  type: number
*                              message:
*                                  type: string
*                              data:
*                                  type: object  
*          500:
*              description: if this process failed
*              content:
*                  application/json:
*                      type: object
* 
*/
routes.post('/test_answers', intrafamilyViolenceTestController.testAnswersViolenceTest);

export default routes;