/**
 * Import Dependencies
 */
import { Request, Response } from "express";
import { PossibleVictim } from "../model/possibleVictim";

/**
 * Import Services
 */
import { PossibleVictimService } from "../service/possibleVictim.service";

/**
 * @class controller possible victim user, for the implementation of HTTP requests
 */
export class PossibleVictimController {

    /**
     * Attributes
     */
    private possibleVictimService: PossibleVictimService;

    /**
     * @constructor
     * Initialize possible victim user service for implement bussiness logic
     */
    public constructor() {
        this.possibleVictimService = new PossibleVictimService();
    }

    /**
     * This method returns the response of whether the login is accepted or not.
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns a json with estate http, message and data
     */
    public signIn = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { email, password } = request.body;
            // login verification
            let resultSession = await this.possibleVictimService.authSessionPossibleVictimUser(email, password);
            return response.status(resultSession.status).json(resultSession);
        } catch (error) {
            console.log("Error in signIn possible victim controller", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }

    /**
     * register new account to possible victim
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns json response to client
     */
    public signUp = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { ci, name, cellphone, email, password, state, type_user, nickname, sex,
                birthday, civil_status, address, profile } = request.body;
            let responseRegister = await this.possibleVictimService.registerNewPossibleVictimUserAccount(
                {
                    ci, name, cellphone, email, password, state, type_user, nickname, sex,
                    birthday, civil_status, address, profile
                }
            );
            return response.status(responseRegister.status).json(responseRegister);
        } catch (error) {
            console.log("Error in possible victim controller register", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }

    /**
     * 
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns json response to client
     */
    public getByEmail = async (request: Request, response: Response): Promise<Response> => {
        try {
            let email = request.params.email;
            let responsePossibleVictimData = await this.possibleVictimService.getPossibleVictimUserByEmail(email);
            return response.status(responsePossibleVictimData.status).json(responsePossibleVictimData);
        } catch (error) {
            console.log("Error in possible victim controller getByEmail", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            })
        }
    }

    public getListTrustedPerson = async (request: Request, response: Response): Promise<Response> => {
        let { ci_victim } = request.params;
        let responseListTrustedPerson = await this.possibleVictimService.getListTrustedPersonOfPossibleVictimAccount(parseInt(ci_victim));
        return response.status(200).json({
            status: 200,
            msg: "get list trusted person success",
            data: responseListTrustedPerson
        });
    }

    public getListTrustedContacts = async (request: Request, response: Response): Promise<Response> => {
        let { ci_victim } = request.params;
        let responseListTrustedContact = await this.possibleVictimService.getListTrustedContactOfPossibleVictimAccount(parseInt(ci_victim));
        return response.status(responseListTrustedContact.status).json(responseListTrustedContact);
    }

    public getListPossibleUsers = async (request: Request, response: Response): Promise<Response> => {
        let { ci_victim } = request.params;
        let responseListUsers = await this.possibleVictimService.getList(parseInt(ci_victim));
        return response.status(responseListUsers.status).json(responseListUsers);
    }

    /**
     * @public
     * restore new account to possible victim manage
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns json response to client
     */
    public restoreAccount = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { email } = request.body;
            let possibleVictim = await this.possibleVictimService.restoreAccount(email);
            return response.status(possibleVictim.status).json(possibleVictim);
        } catch (error) {
            console.log("Error in restoreAccount possibleVictimController", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }
}