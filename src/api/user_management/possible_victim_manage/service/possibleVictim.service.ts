/**
 * Import Components 
 */
import { Encrypt } from "../../../../services/encrypt";
import { AuthJsonWebToken } from '../../../../middlewares/auth';
import { PossibleVictim } from "../model/possibleVictim";

/**
 * Import Models
 */
import { PossibleVictimModel } from "../model/possibleVictim.model";
import { Mail } from "../../../../services/mail";

/**
 * @class of business logic implementation for possible victim manage services users.
 */
export class PossibleVictimService {

    /**
     * Attributes
     */
    private possibleVictimModel: PossibleVictimModel;

    /**
     * @public
     * @constructor
     * initializes the instances of the models dependent on this service.
     */
    public constructor() {
        this.possibleVictimModel = new PossibleVictimModel();
    }

    /**
     * This method does the verification of the police login.
     * @param email : email account to verify
     * @param password : password account to verify
     * @returns result if accept session and return JWT but if denied sesion return message error
     * @catch return an error 500 http to have an error to verify
     */
    public authSessionPossibleVictimUser = async (email: string, password: string): Promise<any> => {
        try {
            let possibleVictimAccount: PossibleVictim | undefined = await this.possibleVictimModel.findByEmail(email);
            if (typeof possibleVictimAccount !== 'undefined') {
                if (await Encrypt.comparePassword(password, possibleVictimAccount.password)) {
                    let jwtSession: string = AuthJsonWebToken.generateToken(possibleVictimAccount.ci, possibleVictimAccount.type_user);
                    return {
                        status: 200,
                        msg: "session success",
                        jwt: jwtSession
                    };
                } else {
                    return {
                        status: 200,
                        msg: "password incorrect"
                    };
                }
            } else {
                return {
                    status: 200,
                    msg: "account not found"
                };
            }
        } catch (error) {
            console.log("Error in possible victim service authSessionPossibleVictimUser()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * Register new account for possible vistim user
     * @param possibleVictim : user pssible victim data to save
     * @returns json response http with jwt if success save
     */
    public registerNewPossibleVictimUserAccount = async (possibleVictim: PossibleVictim): Promise<any> => {
        try {
            possibleVictim.password = await Encrypt.encryptPassword(possibleVictim.password);
            // falta verificar datos del usuario

            // debes verificar datos del usuario
            if (await this.possibleVictimModel.create(possibleVictim)) {
                let jwtSession: string = AuthJsonWebToken.generateToken(possibleVictim.ci, possibleVictim.type_user);
                return {
                    status: 200,
                    msg: "new account saved successuflly",
                    jwt: jwtSession
                }
            } else {
                return {
                    status: 500,
                    msg: "i have problems to register new account"
                };
            }
        } catch (error) {
            console.log("Error in possible victm service register", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * Get possible victim data by email
     * @param email : email account to get
     * @returns json with status, msg and data of a account
     * @catch return a json with status y msg error
     */
    public getPossibleVictimUserByEmail = async (email: string): Promise<any> => {
        try {
            let possibleVictimAccount: PossibleVictim | undefined = await this.possibleVictimModel.findByEmail(email);
            return {
                status: 200,
                msg: "possible victim account",
                data: possibleVictimAccount
            }
        } catch (error) {
            console.log("Error in possible victm service getPossibleVictimUserByEmail()", error);
            return {
                status: 500,
                msg: "internal server error"
            }
        }
    }

    public getListTrustedPersonOfPossibleVictimAccount = async (ciPossibleUser: number): Promise<any> => {
        let listTrustedPerson: Array<PossibleVictim> = await this.possibleVictimModel.getTrustedPersonOfPossibleVictim(ciPossibleUser);
        return {
            status: 200,
            msg: "list trusted person of the victim",
            data: listTrustedPerson
        }
    }

    public getListTrustedContactOfPossibleVictimAccount = async (ciPossibleUser: number): Promise<any> => {
        let listTrustedContact: Array<PossibleVictim> = await this.possibleVictimModel.getTrustedContactOfPossibleVictim(ciPossibleUser);
        return {
            status: 200,
            msg: "list trusted person of the victim",
            data: listTrustedContact
        }
    }

    public getList = async (ciPossibleUser: number) => {
        let listUsers = await this.possibleVictimModel.getList(ciPossibleUser);
        return {
            status: 200,
            msg: "get list success",
            data: listUsers
        }
    }

    public restoreAccount = async (email: string): Promise<any> => {
        try {
            let info = await this.possibleVictimModel.findByEmail(email);
            if (!info) {
                return {
                    status: 200,
                    msg: "no found account registered"
                }
            } else {
                let bodyEmail: string = "send email security";
                if (await Mail.sendMail(info.email, "Restore Account", bodyEmail)) {
                    //if (true) {
                    return {
                        status: 200,
                        msg: "sended email"
                    }
                } else {
                    return {
                        status: 300,
                        msg: "error to send email, again please"
                    }
                }
            }
        } catch (error) {
            console.log("Error in PoliceService restoreAccount()", error);
            return { status: 500, msg: "Internal Server error" };
        }
    }
}