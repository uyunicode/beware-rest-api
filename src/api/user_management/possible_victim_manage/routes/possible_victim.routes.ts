/**
 * Import Libraries
 */
import { Router } from 'express';

/**
 * Import Files
 */
import { AuthJsonWebToken } from '../../../../middlewares/auth';
import { PossibleVictimController } from '../controller/possibleVictim.controller';

/**
 * Import Controllers
 */

// initialize Router
const routes = Router();
// initialize controllers
const possibleVictimController: PossibleVictimController = new PossibleVictimController();

routes.get('/someroute', (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log(req);
    console.log(ip); // ip address of the user
    res.send(ip);
});

// routes http here
/**
 * @swagger
 * /user_management/possible_victim_manage/sign_in :
 *  post:
 *      summary: This performs the process of beginning session (login) for the users possible victim
 *      requestBody:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              email:
 *                                  type: string
 *                              password:
 *                                  type: string
 *      responses:
 *          200:
 *              description: the login verification returns
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              jwt?:
 *                                  type: object
 *                                  description: return jwt if is login success; but another, no returns jwt
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.post('/sign_in', possibleVictimController.signIn);

/**
 * @swagger
 * /user_management/possible_victim_manage/sign_up :
 *  post:
 *      summary: This performs the process register new account possible victim user (mobile user)
 *      requestBody:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              ci:
 *                                  type: number
 *                              name:
 *                                  type: string
 *                              cellphone:
 *                                  type: string
 *                              email:
 *                                  type: string
 *                              password:
 *                                  type: string
 *                              state:
 *                                  type: boolean
 *                              type_user:
 *                                  nickname: string
 *                              sex:
 *                                  type: boolean
 *                              birthday:
 *                                  type: Date
 *                              civil_status:
 *                                  type: string
 *                              address:
 *                                  type: string
 *                              profile:
 *                                  type: string
 *      responses:
 *          200:
 *              description: returns jwt if saved successfully but if not saved send http status 500 or 400
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              jwt?:
 *                                  type: object
 *                                  description: return jwt if is login success; but another, no returns jwt
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.post('/sign_up', possibleVictimController.signUp);

/**
 * @swagger
 * /user_management/possible_victim_manage/:email :
 *  get:
 *      summary: This performs the process get account possible victim user (mobile user) by email
 *      responses:
 *          200:
 *              description: returns possible victim data
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              data:
 *                                  type: object
 *                                  description: if possible victim exists return data account; but another, no returns data account
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.get('/:email', possibleVictimController.getByEmail);

/**
 * @swagger
 * /user_management/possible_victim_manage/get_list_trusted_person/:ci_victim :
 *  get:
 *      summary: get list friends trusted person
 *      responses:
 *          200:
 *              description: returns possible victim data
 *              content:
 *                  application/json:
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.get('/get_list_trusted_person/:ci_victim', possibleVictimController.getListTrustedPerson);

/**
 * @swagger
 * /user_management/possible_victim_manage/get_list_trusted_contact/:ci_victim :
 *  get:
 *      summary: get list friends trusted person
 *      responses:
 *          200:
 *              description: returns possible victim data
 *              content:
 *                  application/json:
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.get('/get_list_trusted_contact/:ci_victim', possibleVictimController.getListTrustedContacts);

/**
 * @swagger
 * /user_management/possible_victim_manage/get_lists/:ci_victim :
 *  get:
 *      summary: get list friends trusted person
 *      responses:
 *          200:
 *              description: returns possible victim data
 *              content:
 *                  application/json:
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.get('/get_lists/:ci_victim', possibleVictimController.getListPossibleUsers);

/**
 * @swagger
 * /user_management/possible_victim_manage/restore_account :
 *  post:
 *      summary: restore new account and send email security
 *      requestBody:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
  *                              email:
 *                                  type: string
 *      responses:
 *          200:
 *              description: send email verification account
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 */
routes.post('/restore_account', possibleVictimController.restoreAccount);

export default routes;