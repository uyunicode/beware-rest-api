/**
 * tipo de dato policia
 */
export interface PossibleVictim {
    ci: number;
    name: string;
    cellphone: string;
    email: string;
    password: string;
    state: boolean;
    type_user: boolean;     // discriinador de usuario (posible victima [true] o policia [false]) 
    nickname: string;
    sex: boolean;           // true: Female, false: Male
    birthday: Date;
    civil_status: string;   // SOL=soltero, CAS=casado, DIV=dicorciado, VIU=viudo, SEP=separado
    address: string;
    profile: string;        // filename of profile
}