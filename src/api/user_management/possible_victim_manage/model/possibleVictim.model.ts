/**
 * Import Dependencies
 */
import { QueryResult } from "pg";
/**
 * Import Components
 */
import { Connection } from "../../../../config/database/connection";
import { PossibleVictim } from "./possibleVictim";


/**
 * @class Model for the administration of possible victim user data.
 */
export class PossibleVictimModel {

    /**
     * Attributes
     */
    public connection: Connection;

    /**
     * @public
     * @constructor
     * Obtain an instance of connection to database
     */
    public constructor() {
        // obtiene la instancia a traves de singleton
        this.connection = Connection.getInstance();
    }

    /**
     * Method that returns information from a possible victim with the identification email.
     * @param email : email to get data info
     * @returns possible victim data or if not find data return undefined
     * @catch undefined if has error in SQLquery
     */
    public findByEmail = async (email: string): Promise<PossibleVictim | undefined> => {
        try {
            // execute sqlQuery
            let userPossibleVictimAccount: QueryResult = await this.connection.executeQuerySQL(`select u.ci, u.name, u.cellphone, u.email, u.password, u.state, u.type_user,pv.nickname,pv.sex,pv.birthday,pv.civil_status,pv.address,pv.profile
                from "user" u, possible_victim pv 
                where u.ci=pv.ci_victim and type_user and state and u.email='${email}'`);
            return userPossibleVictimAccount.rows[0];
        } catch (error) {
            console.log("Error in Possible Victim Model findByEmail", error);
            return undefined;
        }
    }

    /** Method that returns information from a possible victim with the identification CI.
     * @param ciPossibleVictim  : CI to get data info
     * @returns possible victim data or if not find data return undefined
     * @catch undefined if has error in SQLquery
     */
    public findByCI = async (ciPossibleVictim: number): Promise<PossibleVictim | undefined> => {
        try {
            // execute sqlQuery
            let userPossibleVictimAccount: QueryResult = await this.connection.executeQuerySQL(`
                select u.ci, u.name, u.cellphone, u.email, u.password, u.state, u.type_user,pv.nickname,pv.sex,pv.birthday,pv.civil_status,pv.address,pv.profile
                from "user" u, possible_victim pv 
                where u.ci=pv.ci_victim and type_user and state and u.ci='${ciPossibleVictim}'`);
            return userPossibleVictimAccount.rows[0];
        } catch (error) {
            console.log("Error in Possible Victim Model findByCI", error);
            return undefined;
        }
    }

    /**
     * Register new user possible victim
     * @param newPossibleVictim data new mobile user possible victim
     * @returns true if saved succes else false if has a problem to saved or query invalidated
     */
    public create = async (newPossibleVictim: PossibleVictim): Promise<boolean> => {
        try {
            await this.connection.executeQuerySQL(`insert into "user" values 
                (${newPossibleVictim.ci}, '${newPossibleVictim.name}', '${newPossibleVictim.cellphone}', '${newPossibleVictim.email}', '${newPossibleVictim.password}', true, true);
                insert into possible_victim values (${newPossibleVictim.ci}, '${newPossibleVictim.nickname}', ${newPossibleVictim.sex}, 
                    '${newPossibleVictim.birthday}', '${newPossibleVictim.civil_status}', '${newPossibleVictim.address}', '${newPossibleVictim.profile}');`);
            return true;
        } catch (error) {
            console.log("Error in possible victim model ", error);
            return false;
        }
    }

    public getTrustedPersonOfPossibleVictim = async (ci: number): Promise<Array<PossibleVictim>> => {
        try {
            let listUserTristed: QueryResult = await this.connection.executeQuerySQL(
                `select u.ci, u.name, u.cellphone, u.email, u.password, u.state, u.type_user,
                    pv.nickname,pv.sex,pv.birthday,pv.civil_status,pv.address,pv.profile  
                from "user" u, possible_victim pv 
                where u.ci=pv.ci_victim and type_user and u.ci in (
                    select tp.ci_person_trusted
                    from trusted_person tp, "user" u, possible_victim pv
                    where u.ci=pv.ci_victim and pv.ci_victim=tp.ci_victim and u.ci=${ci})`);
            return listUserTristed.rows;
        } catch (error) {
            console.log("Error in getTrustedPersonOfPossibleVictim(): ", error);
            return [];
        }
    }

    public getTrustedContactOfPossibleVictim = async (ci: number): Promise<Array<any>> => {
        try {
            let listUserTrusted: QueryResult = await this.connection.executeQuerySQL(
                `select c.cellphone, c.email, c."name" 
                from possible_victim pv, contact c, trusted_contact tc
                where pv.ci_victim=tc.ci_victim and tc.cellphone_trusted_contact=c.cellphone and pv.ci_victim='${ci}';`);
            return listUserTrusted.rows;
        } catch (error) {
            console.log("Error in getTrustedPersonOfPossibleVictim(): ", error);
            return [];
        }
    }

    public getList = async (ci: number): Promise<Array<any>> => {
        let result = await this.connection.executeQuerySQL(
            `select * from possible_victim where ci_victim!=${ci}`);
        return result.rows;
    }
}