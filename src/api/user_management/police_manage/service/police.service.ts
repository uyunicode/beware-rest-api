/**
 * Import Components 
 */
import { Encrypt } from "../../../../services/encrypt";
import { Police } from "../model/police";
import { AuthJsonWebToken } from '../../../../middlewares/auth';
/**
 * Import Models
 */
import { PoliceModel } from "../model/police.model";
import { Mail } from "../../../../services/mail";

/**
 * @class of business logic implementation for police management services users.
 */
export class PoliceService {

    /**
     * Attributes
     */
    private policeModel: PoliceModel;

    /**
     * @public
     * @constructor
     * initializes the instances of the models dependent on this service.
     */
    public constructor() {
        this.policeModel = new PoliceModel();
    }

    /**
     * This method does the verification of the police login.
     * @param email : email account to verify
     * @param password : password account to verify
     * @returns result if accept session and return JWT but if denied sesion return message error
     * @catch return an error 500 http to have an error to verify
     */
    public authSessionPoliceUser = async (email: string, password: string) => {
        try {
            let policeAccount: Police | undefined = await this.policeModel.findByEmail(email);
            if (typeof policeAccount !== 'undefined') {
                if (!await Encrypt.comparePassword(policeAccount.password, password)) {
                    let jwtSession: string = AuthJsonWebToken.generateToken(policeAccount.ci, policeAccount.type_user);
                    return {
                        status: 200,
                        msg: "Session success",
                        jwt: jwtSession,
                        data: policeAccount
                    };
                } else {
                    return {
                        status: 200,
                        msg: "Password Incorrect"
                    };
                }
            } else {
                return {
                    status: 200,
                    msg: "Account not found"
                };
            }
        } catch (error) {
            console.log("Error in PoliceService authSessionPoliceUser()", error);
            return {
                status: 500,
                msg: "Internal Server error"
            };
        }
    }

    /**
     * Get list police user
     * @returns JSON info
     */
    public getListPoliceUser = async () => {
        try {
            let listPolice = await this.policeModel.findByAll();
            return {
                status: 200,
                msg: listPolice.length > 0 ? "get list police success" : "Doesn't have polices account",
                data: listPolice
            }
        } catch (error) {
            console.log("Error in get list police user");
            return {
                status: 500,
                msg: "Internal Server error"
            };
        }
    }

    /**
     * @public
     * register new account for police user
     * @param police : data of a new police user
     * @returns json response http with jwt if success save
     */
    public registerPolice = async (police: Police): Promise<any> => {
        try {
            police.password = await Encrypt.encryptPassword(police.password);
            if (await this.policeModel.create(police)) {
                let jwtSession: string = AuthJsonWebToken.generateToken(police.ci, police.type_user);
                return {
                    status: 200,
                    msg: "new police account saved successuflly",
                    jwt: jwtSession
                }
            } else {
                return {
                    status: 500,
                    msg: "i have problems to register new police account"
                };
            }

        } catch (error) {
            console.log("Error in PoliceService registerPolice()", error);
            return { status: 500, msg: "Internal Server error" };
        }
    }

    public restoreAccount = async (email: string): Promise<any> => {
        try {
            let info = await this.policeModel.findByEmail(email);
            if (!info) {
                return {
                    status: 200,
                    msg: "no found account registered"
                }
            } else {
                let bodyEmail: string = "send email security";
                if (await Mail.sendMail(info.email, "Restore Account", bodyEmail)) {
                    //if (true) {
                    return {
                        status: 200,
                        msg: "sended email"
                    }
                } else {
                    return {
                        status: 300,
                        msg: "error to send email, again please"
                    }
                }
            }
        } catch (error) {
            console.log("Error in PoliceService restoreAccount()", error);
            return { status: 500, msg: "Internal Server error" };
        }
    }
}