/**
 * Import Libraries
 */
import { Router } from 'express';

/**
 * Import Files
 */
import { AuthJsonWebToken } from '../../../../middlewares/auth';

/**
 * Import Controllers
 */
import { PoliceController } from '../controller/police.controller';

// initialize Router
const routes = Router();
// initialize controllers
const policeController: PoliceController = new PoliceController();

routes.get('/someroute', (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log(req);
    console.log(ip); // ip address of the user
    res.send(ip);
});

// routes http here
/**
 * @swagger
 * /user_management/police_manage/sign_in :
 *  post:
 *      summary: This performs the process of beginning session (login) for the users police
 *      requestBody:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              email:
 *                                  type: string
 *                              password:
 *                                  type: string
 *      responses:
 *          200:
 *              description: the login verification returns
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              jwt?:
 *                                  type: object
 *                                  description: return jwt if is login success; but another, no returns jwt
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 */
routes.post('/sign_in', policeController.signIn);

/**
 * @swagger
 * /user_management/police_manage/list_police_user :
 *  post:
 *      summary: This performs the process to get list account police user
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: the login verification returns
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              jwt?:
 *                                  type: object
 *                                  description: return jwt if is login success; but another, no returns jwt
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 */
routes.get('/list_police_user', policeController.getListPoliceAccount);

/**
 * @swagger
 * /user_management/police_manage/sign_up :
 *  post:
 *      summary: create a new police user account
 *      requestBody:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              ci:
 *                                  type: number
 *                              name:
 *                                  type: string
 *                              cellphone:
 *                                  type: string
 *                              email:
 *                                  type: string
 *                              password:
 *                                  type: string
 *                              state:
 *                                  type: boolean
 *                              type_user:
 *                                  type: boolean
 *                              ladder_number:
 *                                  type: string
 *                              grade:
 *                                  type: string
 *      responses:
 *          200:
 *              description: returns a json if saved
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              jwt?:
 *                                  type: string
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 */
routes.post('/sign_up', policeController.signUp);

/**
 * @swagger
 * /user_management/police_manage/restore_account :
 *  post:
 *      summary: restore new account and send email security
 *      requestBody:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
  *                              email:
 *                                  type: string
 *      responses:
 *          200:
 *              description: send email verification account
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 */
routes.post('/restore_account', policeController.restoreAccount);

// routes.post('/restore_account', authController.restoreAccount);

// routes.post('/confirm_key', authController.confirmKey);

// routes.post('/new_password', authController.restoreNewPassword);

export default routes;