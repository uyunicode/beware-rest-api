/**
 * tipo de dato policia
 */
export interface Police {
    ci: number;
    name: string;
    cellphone: string;
    email: string;
    password: string;
    state: boolean;
    type_user: boolean;
    ladder_number: string;
    grade: string;
}