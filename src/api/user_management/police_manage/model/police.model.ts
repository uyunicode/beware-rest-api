/**
 * Import Dependencies
 */
import { QueryResult } from "pg";
/**
 * Import Components
 */
import { Connection } from "../../../../config/database/connection";
import { Police } from './police';

/**
 * @class Model for the administration of police user data.
 */
export class PoliceModel {

    /**
     * Attributes
     */
    public connection: Connection;

    /**
     * @public
     * @constructor
     * Obtain an instance of connection to database
     */
    public constructor() {
        // obtiene la instancia a traves de singleton
        this.connection = Connection.getInstance();
    }

    /**
     * Get list police user storage
     */
    public findByAll = async (): Promise<Array<Police>> => {
        try {
            // execute sqlQuery
            let listUserPolice: QueryResult = await this.connection.executeQuerySQL(`
            select u.ci, u."name", u.cellphone, u.email, u."password", u.state, u.type_user, p.ladder_number, p.grade 
                from "user" u, police p 
                where u.ci=p.ci_police and not type_user`);
            return listUserPolice.rows;
        } catch (error) {
            console.log("Error in Police Model findByEmail", error);
            return [];
        }
    }

    /**
     * Method that returns information from a policeman with the identification email.
     * @param email : email to get data info
     * @returns police data or if not find data return undefined
     * @catch undefined if has error in SQLquery
     */
    public findByEmail = async (email: string): Promise<Police | undefined> => {
        try {
            // execute sqlQuery
            let userPoliceAccount: QueryResult = await this.connection.executeQuerySQL(`select u.ci, u.name, u.cellphone, u.email, u.password, u.state, u.type_user, p.ladder_number, p.grade 
                from "user" u, police p 
                where p.ci_police=u.ci and not u.type_user and u.email='${email}' and u.state`);
            return userPoliceAccount.rows[0];
        } catch (error) {
            console.log("Error in Police Model findByEmail", error);
            return undefined;
        }
    }

    /**
     * @public
     * Register new user police
     * @param newPolice data new web user police
     * @returns data new police if saved succes else  if has a problem to saved or query invalidated
     */
     public create = async (newPolice: Police): Promise<boolean> => {
        try {
            await this.connection.executeQuerySQL(`insert into "user" values 
                (${newPolice.ci}, '${newPolice.name}', '${newPolice.cellphone}', '${newPolice.email}', '${newPolice.password}', true, ${newPolice.type_user});
                insert into police (ci_police, ladder_number, grade) values(${newPolice.ci}, '${newPolice.ladder_number}', '${newPolice.grade}');`);
            return true;
        } catch (error) {
            console.log("Error in police model ", error);
            return false;
        }
    }

    // public findByID(id: any): Police { return; }
    // public create(data: T): boolean { }
    // public update(newData: T): boolean { }
    // public delete(data: T): boolean { }
}