/**
 * Import Dependencies
 */
import { Request, Response } from "express";

/**
 * Import Services
 */
import { PoliceService } from "../service/police.service";

/**
 * @class controller police, for the implementation of HTTP requests
 */
export class PoliceController {

    /**
     * Attributes
     */
    private policeService: PoliceService;

    /**
     * @constructor
     * Initialize police service for implement bussiness logic
     */
    public constructor() {
        this.policeService = new PoliceService();
    }

    /**
     * This method returns the response of whether the login is accepted or not.
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns a json with estate http, message and data
     */
    public signIn = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { email, password } = request.body;
            // verification de login
            let resultSession = await this.policeService.authSessionPoliceUser(email, password);
            return response.status(resultSession.status).json(resultSession);
        } catch (error) {
            console.log("Error in signIn policeController", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            })
        }
    }

    /**
     * This method returns the response of list police user accounts
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns a json with estate http, message and data
     */
    public getListPoliceAccount = async (request: Request, response: Response): Promise<Response> => {
        try {
            let listPoliceUserAccount = await this.policeService.getListPoliceUser();
            return response.status(listPoliceUserAccount.status).json(listPoliceUserAccount);
        } catch (error) {
            console.log("Error in signIn policeController", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            })
        }
    }

    /**
     * @public
     * register new account to police
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns json response to client
     */
    public signUp = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { ci, name, cellphone, email, password, state, type_user, ladder_number, grade } = request.body;
            let police = await this.policeService.registerPolice({ ci, name, cellphone, email, password, state, type_user, ladder_number, grade });
            return response.status(police.status).json(police);
        } catch (error) {
            console.log("Error in signIn policeController", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }

    /**
     * @public
     * restore new account to police manage
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns json response to client
     */
    public restoreAccount = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { email } = request.body;
            let police = await this.policeService.restoreAccount(email);
            return response.status(police.status).json(police);
        } catch (error) {
            console.log("Error in restoreAccount policeController", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }
}