/**
 * Import Dependencies
 */
import { Request, Response } from "express";

/**
 * Import Services
 */
import { VictimizerService } from "../service/victimizer.service";

/**
 * @class controller victimizer, for the implementation of HTTP requests
 */
export class VictimizerController {

    /**
     * Attributes
     */
    private victimizerService: VictimizerService;

    /**
     * @constructor
     * Initialize possible victim user service for implement bussiness logic
     */
    public constructor() {
        this.victimizerService = new VictimizerService();
    }

    /**
     * add trusted person of possible victim
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns a json with estate http, message and data
     */
    public addVictimizerInfo = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { ci, name} = request.body;
            let file: Buffer| undefined= request.file?.buffer;
            let result = await this.victimizerService.addVictimizerInfo(ci, name, file);
            return response.status(result.status).json(result);
        } catch (error) {
            console.log("Error in add VictimizerController: ", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }

    /**
     * remove trusted person of possible victim
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns a json with estate http, message and data
     */
    public getListVictimizer = async (request: Request, response: Response): Promise<Response> => {
        try {
            let listVictimizerInfo = await this.victimizerService.getListVictimizers();
            return response.status(listVictimizerInfo.status).json(listVictimizerInfo);
        } catch (error) {
            console.log("Error in remove VictimizerController: method: getListVictmizer: ", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }
}