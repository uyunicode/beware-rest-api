/**
 * Import Components
 */
import { Connection } from "../../../../config/database/connection";
import { Victimizer } from "./victimizer";


/**
 * @class Model for friend trusted or family's victim manage
 */
export class VictimizerModel {

    /**
     * Attributes
     */
    public connection: Connection;

    /**
     * @public
     * @constructor
     * Obtain an instance of connection to database
     */
    public constructor() {
        // obtiene la instancia a traves de singleton
        this.connection = Connection.getInstance();
    }

    /**
     * Add new victimizer
     * @param victimizer : info victimizer save to database
     * @returns true if added successfully else returns false
     */
    public create = async (victimizer: Victimizer): Promise<boolean> => {
        try {
            await this.connection.executeQuerySQL(
                `insert into victimizer (ci, "name", profile) values(${victimizer.ci}, '${victimizer.name}', '${victimizer.profile}');`);
            return true;
        } catch (error) {
            console.log("Error in VictimizerModel create() method: ", error);
            return false;
        }
    }

    /**
     * remove trusted person
     * @param Victimizer : CI possible victim and CI trusted person
     * @returns true if remove successfully else returns false
     */
    public findAll = async (): Promise<Array<Victimizer>> => {
        try {
            let listVictimizerSaved = await this.connection.executeQuerySQL(`select ci, name, profile from victimizer;`);
            return listVictimizerSaved.rows;
        } catch (error) {
            console.log("Error in VictimizerModel: ", error);
            return [];
        }
    }

    /**
     * @public
     * find a victimizer by profile
     * @param profile : name of file of photo of victimizer
     * @returns victimizer data else undefined
     */
     public findByProfile = async (profile: string): Promise<Victimizer| undefined> => {
        try {
            console.log(`profile:  ${profile}`);
            let victimizerByProfile = await this.connection.executeQuerySQL(`select * from victimizer where profile='${profile}';`);
            return victimizerByProfile.rows[0];
        } catch (error) {
            console.log("Error in VictimizerModel: ", error);
            return undefined;
        }
    }

}