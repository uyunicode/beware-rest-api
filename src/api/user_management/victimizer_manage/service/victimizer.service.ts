/**
 * Import Components 
 */
import { AwsS3Client } from '../../../../services/aws_s3';
import { AwsRekognitionClient } from '../../../../services/aws_rekognition';
import { Victimizer } from "../model/victimizer";
import { VictimizerModel } from "../model/victimizer.model";

/**
 * Import Models
 */

/**
 * @class of business logic implementation for victimizer profiles.
 */
export class VictimizerService {

    /**
     * Attributes
     */
    private victimizerModel: VictimizerModel;

    /**
     * @public
     * @constructor
     * initializes the instances of the models dependent on this service.
     */
    public constructor() {
        this.victimizerModel = new VictimizerModel();
    }

    /**
     * add new victimizer info
     * @param ci : ci identifier of victimizer
     * @param name : name of the victimizer
     * @param profile : profile file name of victimizer
     * @returns : JSON data response
     */
    public addVictimizerInfo = async (ci: number, name: string, file: Buffer| undefined): Promise<any> => {
        try {
            let awsClient: AwsS3Client = new AwsS3Client();
            let profile: string= await awsClient.sendObject(file, "victimizer-profiles/");
            let awsRekognitionClient: AwsRekognitionClient = new AwsRekognitionClient();
            awsRekognitionClient.addingFacesToACollection(profile);
            if (await this.victimizerModel.create({ ci, name, profile })) {
                return {
                    status: 200,
                    msg: "saved success victimizer info"
                };
            } else {
                return {
                    status: 200,
                    msg: "have an error to save victimizer info"
                };
            }
        } catch (error) {
            console.log("Error in victimizerService addVictimizerInfo()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * Get list victimizer info
     * @returns list victimizer
     */
    public getListVictimizers = async (): Promise<any> => {
        try {
            let listVictimizer: Array<Victimizer> = await this.victimizerModel.findAll();
            if (listVictimizer.length > 0) {
                return {
                    status: 200,
                    msg: "get list victimizer success",
                    data: listVictimizer
                };
            } else {
                return {
                    status: 200,
                    msg: "get list victimizer empty",
                    data: listVictimizer
                };
            }
        } catch (error) {
            console.log("Error in possible victim service getListVictimizer()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }
}