/**
 * Import Libraries
 */
import { Router } from 'express';
import multer from 'multer';

/**
 * Import Files
 */
import { VictimizerController } from '../controller/victimizer.controller';

/**
 * Import Controllers
 */

// initialize Router
const routes = Router();
// initialize multer
var upload = multer({storage: multer.memoryStorage()});
// initialize controllers
let victimizerController = new VictimizerController();

/**
  * @swagger
  * /user_management/victimizer_manage/add_victimizer :
  *  post:
  *      summary: add new victimizer
  *      requestBody:
  *              content:
  *                  application/json:
  *      responses:
  *          200:
  *              description: added successfully
  *              content:
  *                  application/json:
  *                      schema:
  *                          type: object
  *                          properties:
  *                              state:
  *                                  type: number
  *                              message:
  *                                  type: string
  *                                  data: any
  *          500:
  *              description: if this process failed
  *              content:
  *                  application/json:
  *                      type: object
  * 
  */
routes.post('/add_victimizer', upload.single('profile'), victimizerController.addVictimizerInfo);

/**
 * @swagger
 * /user_management/victimizer_manage/get_list_victimizer :
 *  post:
 *      summary: This performs the process get list victimizer saved to database
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: obtained successfully
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.get('/get_list_victimizer', victimizerController.getListVictimizer);

export default routes;