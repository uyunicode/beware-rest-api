/**
 * Import Components
 */
import { Connection } from "../../../../config/database/connection";
import { TrustedPerson } from "./trustedPerson";


/**
 * @class Model for friend trusted or family's victim manage
 */
export class TrustedPersonModel {

    /**
     * Attributes
     */
    public connection: Connection;

    /**
     * @public
     * @constructor
     * Obtain an instance of connection to database
     */
    public constructor() {
        // obtiene la instancia a traves de singleton
        this.connection = Connection.getInstance();
    }

    /**
     * Add trusted person
     * @param trustedPerson : CI possible victim and CI trusted person
     * @returns true if added successfully else returns false
     */
    public addTrustedPerson = async (trustedPerson: TrustedPerson): Promise<boolean> => {
        try {
            await this.connection.executeQuerySQL(
                `insert into trusted_person(ci_victim,ci_person_trusted) values(${trustedPerson.ci_victim}, ${trustedPerson.ci_person_trusted});`);
            return true;
        } catch (error) {
            console.log("Error in TrustedPersonModel: ", error);
            return false;
        }
    }

    /**
     * remove trusted person
     * @param trustedPerson : CI possible victim and CI trusted person
     * @returns true if remove successfully else returns false
     */
    public removeTrustedPerson = async (trustedPerson: TrustedPerson): Promise<boolean> => {
        try {
            await this.connection.executeQuerySQL(
                `delete from trusted_person where ci_victim=${trustedPerson.ci_victim} and ci_person_trusted=${trustedPerson.ci_person_trusted}`);
            return true;
        } catch (error) {
            console.log("Error in TrustedPersonModel: ", error);
            return false;
        }
    }
}