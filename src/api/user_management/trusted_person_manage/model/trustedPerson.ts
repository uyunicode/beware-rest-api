export interface TrustedPerson {
    ci_victim: number;
    ci_person_trusted: number;
}