/**
 * Import Dependencies
 */
import { Request, Response } from "express";

/**
 * Import Services
 */
import { TrustedPersonService } from "../service/trustedPerson.service";

/**
 * @class controller possible victim user, for the implementation of HTTP requests
 */
export class TrustedPersonController {

    /**
     * Attributes
     */
    private trustedPersonService: TrustedPersonService;

    /**
     * @constructor
     * Initialize possible victim user service for implement bussiness logic
     */
    public constructor() {
        this.trustedPersonService = new TrustedPersonService();
    }

    /**
     * add trusted person of possible victim
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns a json with estate http, message and data
     */
    public addTrustedPerson = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { ci_victim, ci_person_trusted } = request.body;
            let result = await this.trustedPersonService.addTrustedPerson(ci_victim, ci_person_trusted);
            return response.status(result.status).json(result);
        } catch (error) {
            console.log("Error in add TrustedPersonController: ", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }

    /**
     * remove trusted person of possible victim
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns a json with estate http, message and data
     */
    public removeTrustedPerson = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { ci_victim, ci_person_trusted } = request.body;
            let result = await this.trustedPersonService.removeTrustedPerson(ci_victim, ci_person_trusted);
            return response.status(result.status).json(result);
        } catch (error) {
            console.log("Error in remove TrustedPersonController: ", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }
}