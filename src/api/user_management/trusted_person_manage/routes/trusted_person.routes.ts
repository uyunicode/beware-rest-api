/**
 * Import Libraries
 */
import { Router } from 'express';

/**
 * Import Files
 */
import { AuthJsonWebToken } from '../../../../middlewares/auth';
import { TrustedPersonController } from '../controller/trustedPerson.controller';

/**
 * Import Controllers
 */

// initialize Router
const routes = Router();
// initialize controllers
let trustedPersonController = new TrustedPersonController();

/**
  * @swagger
  * /user_management/trusted_person_manage/add_trusted_person :
  *  post:
  *      summary: add new trusted person
  *      requestBody:
  *              content:
  *                  application/json:
  *      responses:
  *          200:
  *              description: added successfully
  *              content:
  *                  application/json:
  *                      schema:
  *                          type: object
  *                          properties:
  *                              state:
  *                                  type: number
  *                              message:
  *                                  type: string
  *          500:
  *              description: if this process failed
  *              content:
  *                  application/json:
  *                      type: object
  * 
  */
routes.post('/add_trusted_person', trustedPersonController.addTrustedPerson);

/**
 * @swagger
 * /user_management/trusted_person_manage/remove_trusted_person :
 *  post:
 *      summary: This performs the process register new account possible victim user (mobile user)
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: removed successfully
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.post('/remove_trusted_person', trustedPersonController.removeTrustedPerson);

export default routes;