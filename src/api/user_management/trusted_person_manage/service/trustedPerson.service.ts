/**
 * Import Components 
 */
import { Encrypt } from "../../../../services/encrypt";
import { AuthJsonWebToken } from '../../../../middlewares/auth';
import { TrustedPersonModel } from "../model/trustedPerson.model";

/**
 * Import Models
 */

/**
 * @class of business logic implementation for possible victim manage services users.
 */
export class TrustedPersonService {

    /**
     * Attributes
     */
    private trustedPersonModel: TrustedPersonModel;

    /**
     * @public
     * @constructor
     * initializes the instances of the models dependent on this service.
     */
    public constructor() {
        this.trustedPersonModel = new TrustedPersonModel();
    }

    /**
     * add new trusted person to possible victim
     * @param ci_victim : victim CI
     * @param ci_person_trusted : Trusted Person CI
     * @returns response json
     */
    public addTrustedPerson = async (ci_victim: number, ci_person_trusted: number): Promise<any> => {
        try {
            if (await this.trustedPersonModel.addTrustedPerson({ ci_victim, ci_person_trusted })) {
                return {
                    status: 200,
                    msg: "added trusted person success"
                };
            } else {
                return {
                    status: 500,
                    msg: "error in add trusted person"
                };
            }
        } catch (error) {
            console.log("Error in possible victim service addTrustedPerson()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * remove existent trusted person of possible victim
     * @param ci_victim : victim CI
     * @param ci_person_trusted : Trusted Person CI
     * @returns response json
     */
    public removeTrustedPerson = async (ci_victim: number, ci_person_trusted: number): Promise<any> => {
        try {
            if (await this.trustedPersonModel.removeTrustedPerson({ ci_victim, ci_person_trusted })) {
                return {
                    status: 200,
                    msg: "remove trusted person success"
                };
            } else {
                return {
                    status: 500,
                    msg: "error in remove trusted person"
                };
            }
        } catch (error) {
            console.log("Error in possible victim service removeTrustedPerson()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }
}