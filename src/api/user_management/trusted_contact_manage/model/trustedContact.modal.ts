/**
 * Import Components
 */
import { Connection } from "../../../../config/database/connection";
import { TrustedContact } from "./trustedContact";



/**
 * @class Model for friend trusted or family's victim manage
 */
export class TrustedContactModel {

    /**
     * Attributes
     */
    public connection: Connection;

    /**
     * @public
     * @constructor
     * Obtain an instance of connection to database
     */
    public constructor() {
        // obtiene la instancia a traves de singleton
        this.connection = Connection.getInstance();
    }

    /**
     * Add trusted person
     * @param trustedContact : CI possible victim and CI trusted person
     * @returns true if added successfully else returns false
     */
    public addTrustedContact = async (trustedContact: TrustedContact): Promise<boolean> => {
        try {
            await this.connection.executeQuerySQL(
                `insert into trusted_contact(ci_victim,cellphone_trusted_contact) values
                    (${trustedContact.ci_victim}, '${trustedContact.cellphone_trusted_contact}');`);
            return true;
        } catch (error) {
            console.log("Error in TrustedContactModel: ", error);
            return false;
        }
    }

    /**
     * remove trusted person
     * @param trustedContact : CI possible victim and CI trusted person
     * @returns true if remove successfully else returns false
     */
    public removetrustedContact = async (trustedContact: TrustedContact): Promise<boolean> => {
        try {
            await this.connection.executeQuerySQL(
                `delete from trusted_contact where ci_victim=${trustedContact.ci_victim} and cellphone_trusted_contact='${trustedContact.cellphone_trusted_contact}';`);
            return true;
        } catch (error) {
            console.log("Error in TrustedContactModel: ", error);
            return false;
        }
    }
}