export interface TrustedContact {
    ci_victim: number;
    cellphone_trusted_contact: string;
}