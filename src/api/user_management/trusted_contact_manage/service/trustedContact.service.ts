/**
 * Import Components 
 */
import { Encrypt } from "../../../../services/encrypt";
import { AuthJsonWebToken } from '../../../../middlewares/auth';
import { TrustedContactModel } from "../model/trustedContact.modal";


/**
 * Import Models
 */

/**
 * @class of business logic implementation for possible victim manage services users.
 */
export class TrustedContactService {

    /**
     * Attributes
     */
    private trustedContactModel: TrustedContactModel;

    /**
     * @public
     * @constructor
     * initializes the instances of the models dependent on this service.
     */
    public constructor() {
        this.trustedContactModel = new TrustedContactModel();
    }

    /**
     * add new trusted person to possible victim
     * @param ci_victim : victim CI
     * @param cellphone_trusted_contact : phone contact
     * @returns response json
     */
    public addTrustedContact = async (ci_victim: number, cellphone_trusted_contact: string): Promise<any> => {
        try {
            if (await this.trustedContactModel.addTrustedContact({ ci_victim, cellphone_trusted_contact })) {
                return {
                    status: 200,
                    msg: "added trusted contact success"
                };
            } else {
                return {
                    status: 500,
                    msg: "error in add trusted person"
                };
            }
        } catch (error) {
            console.log("Error in possible victim service addTrustedContact()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * remove existent trusted person of possible victim
     * @param ci_victim : victim CI
     * @param ci_person_trusted : Trusted Person CI
     * @returns response json
     */
    public removeTrustedContact = async (ci_victim: number, cellphone_trusted_contact: string): Promise<any> => {
        try {
            if (await this.trustedContactModel.removetrustedContact({ ci_victim, cellphone_trusted_contact })) {
                return {
                    status: 200,
                    msg: "remove trusted contact success"
                };
            } else {
                return {
                    status: 500,
                    msg: "error in remove trusted contact"
                };
            }
        } catch (error) {
            console.log("Error in possible victim service removeTrustedContact()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }
}