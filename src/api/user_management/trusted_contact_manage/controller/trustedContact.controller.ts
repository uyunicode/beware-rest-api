/**
 * Import Dependencies
 */
import { Request, Response } from "express";
import { TrustedContactService } from "../service/trustedContact.service";

/**
 * Import Services
 */

/**
 * @class controller possible victim user, for the implementation of HTTP requests
 */
export class TrustedContactController {

    /**
     * Attributes
     */
    private trustedContactService: TrustedContactService;

    /**
     * @constructor
     * Initialize possible victim user service for implement bussiness logic
     */
    public constructor() {
        this.trustedContactService = new TrustedContactService();
    }

    /**
     * add trusted person of possible victim
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns a json with estate http, message and data
     */
    public addTrustedContact = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { ci_victim, cellphone_trusted_contact } = request.body;
            let result = await this.trustedContactService.addTrustedContact(ci_victim, cellphone_trusted_contact);
            return response.status(result.status).json(result);
        } catch (error) {
            console.log("Error in add TrustedPersonController: ", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }

    /**
     * remove trusted person of possible victim
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns a json with estate http, message and data
     */
    public removeTrustedContact = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { ci_victim, cellphone_trusted_contact } = request.body;
            let result = await this.trustedContactService.removeTrustedContact(ci_victim, cellphone_trusted_contact);
            return response.status(result.status).json(result);
        } catch (error) {
            console.log("Error in remove TrustedPersonController: ", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }
}