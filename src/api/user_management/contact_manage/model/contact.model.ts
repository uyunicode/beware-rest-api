/**
 * Import Dependencies
 */
import { QueryResult } from "pg";
/**
 * Import Components
 */
import { Connection } from "../../../../config/database/connection";
import { Contact } from "./contact";


/**
 * @class Model for the administration of possible victim user data.
 */
export class ContactModel {

    /**
     * Attributes
     */
    public connection: Connection;

    /**
     * @public
     * @constructor
     * Obtain an instance of connection to database
     */
    public constructor() {
        // obtiene la instancia a traves de singleton
        this.connection = Connection.getInstance();
    }

    /**
     * Method that returns information from a possible victim with the identification email.
     * @param email : email to get data info
     * @returns possible victim data or if not find data return undefined
     * @catch undefined if has error in SQLquery
     */
    public create = async (contact: Contact): Promise<boolean> => {
        try {
            // execute sqlQuery
            await this.connection.executeQuerySQL(
                `insert into contact(cellphone,email,"name") values ('${contact.cellphone}', '${contact.email}', '${contact.name}')`);
            return true;
        } catch (error) {
            console.log("Error in Possible Victim Model findByEmail", error);
            return false;
        }
    }

    /**
     * get list contacts of possible victim
     * @param ciPossibleVictim : ci possible victim
     * @returns list contacts
     */
    public getListContactsOfPossibleVictim = async (ciPossibleVictim: number): Promise<Array<Contact>> => {
        try {
            let listContact: Array<Contact> = (await this.connection.executeQuerySQL(
                `select c.cellphone, c.email, c."name" 
                from possible_victim pv, contact c, trusted_contact tc
                where pv.ci_victim=tc.ci_victim and tc.cellphone_trusted_contact=c.cellphone and pv.ci_victim='${ciPossibleVictim}';`)).rows;
            return listContact;
        } catch (error) {
            console.log("Error in method getListContactsOfPossibleVictim: " + error);
            return [];
        }

    }
}