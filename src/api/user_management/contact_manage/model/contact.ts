export interface Contact {
    cellphone: string;
    email: string;
    name: string;
}