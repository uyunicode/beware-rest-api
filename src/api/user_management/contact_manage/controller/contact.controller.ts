/**
 * Import Dependencies
 */
import { Request, Response } from "express";
import { ContactService } from "../service/contact.service";

/**
 * Import Services
 */

/**
 * @class controller possible victim user, for the implementation of HTTP requests
 */
export class ContactController {

    /**
     * Attributes
     */
    private contactService: ContactService;

    /**
     * @constructor
     * Initialize possible victim user service for implement bussiness logic
     */
    public constructor() {
        this.contactService = new ContactService();
    }

    /**
     * This method returns the response of whether the login is accepted or not.
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns a json with estate http, message and data
     */
    public createContact = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { cellphone, email, name } = request.body;
            let result = await this.contactService.createContact({ cellphone, email, name });
            return response.status(result.status).json(result);
        } catch (error) {
            console.log("Error in createContact contact controller", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }

    public getListContactsOfPossibleVictim = async (request: Request, response: Response): Promise<Response> => {
        let { ci_victim } = request.params;
        let result = await this.contactService.getListContactsOfPossibleVictim(parseInt(ci_victim));
        return response.status(result.status).json(result);
    }
}