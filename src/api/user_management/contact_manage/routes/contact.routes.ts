/**
 * Import Libraries
 */
import { Router } from 'express';

/**
 * Import Files
 */
import { AuthJsonWebToken } from '../../../../middlewares/auth';
import { ContactController } from '../controller/contact.controller';

/**
 * Import Controllers
 */

// initialize Router
const routes = Router();
// initialize controllers
const contactController: ContactController = new ContactController();

/**
 * @swagger
 * /user_management/contact_manage/create_contact :
 *  post:
 *      summary: create contact
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: the login verification returns
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.post('/create_contact', contactController.createContact);

/**
 * @swagger
 * /user_management/contact_manage/get_list_contacts/:ci_victim :
 *  get:
 *      summary: get contacts of a possible victim
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: returns jwt if saved successfully but if not saved send http status 500 or 400
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              data:
 *                                  type: array
 *                                  description: list contacts
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.get('/get_list_contacts/:ci_victim', contactController.getListContactsOfPossibleVictim);

export default routes;