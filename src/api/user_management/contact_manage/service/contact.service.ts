/**
 * Import Components 
 */
import { Encrypt } from "../../../../services/encrypt";
import { AuthJsonWebToken } from '../../../../middlewares/auth';
import { ContactModel } from "../model/contact.model";
import { Contact } from "../model/contact";

/**
 * Import Models
 */


/**
 * @class of business logic implementation for possible victim manage services users.
 */
export class ContactService {

    /**
     * Attributes
     */
    private contactModel: ContactModel;

    /**
     * @public
     * @constructor
     * initializes the instances of the models dependent on this service.
     */
    public constructor() {
        this.contactModel = new ContactModel();
    }

    public createContact = async (contact: Contact): Promise<any> => {
        try {
            if (await this.contactModel.create(contact)) {
                return {
                    status: 200,
                    msg: "created contact successfully"
                };
            } else {
                return {
                    status: 500,
                    msg: "error in create contact"
                };
            }
        } catch (error) {
            console.log("Error in Contact service createContact()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    public getListContactsOfPossibleVictim = async (ciPossibleVictim: number): Promise<any> => {
        try {
            let listContactsOfVictim = await this.contactModel.getListContactsOfPossibleVictim(ciPossibleVictim);
            return {
                status: 200,
                msg: "new account saved successuflly",
                data: listContactsOfVictim
            }
        } catch (error) {
            console.log("Error in get List Contacts Of Possible Victim", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }
}