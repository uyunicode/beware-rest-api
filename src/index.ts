/**
 * @internal
 * Documentation of this project is provided by the dependencies of
 * @see {@link https://typedoc.org/} source code documentation
 * @see {@link https://swagger.io/} documentation of requests or web services
 * @see {@link https://swagger.io/docs/specification/basic-structure/} for read http request in swagger
 * * Desarrolladores
 * @username {Cruz Rodriguez Zuleny, Quispe Mamani Ruddy Bryan}
 * * Software Engineering 2 - Degree Workshop
 * * UAGRM University
 */
import { App } from './app'

/**
 * @public
 * Web server compilation method
 * Start the HTTP service
 */
async function main(): Promise<void> {
    const app = new App(3000);
    await app.listen();
}

// execution of the main method
main();