-- materia: ingeniería en software 2 - taller de grado
-- uagrm - ficct
-- @author: cruz rodriguez zuleny, ruddy bryan quispe mamani 
-- @version: 0.0.1
-- @since: 23-05-2021

-- ******** CREACION DEL USUARIO O ROL ******** 

CREATE ROLE beware_software
   WITH
   LOGIN
   INHERIT
   REPLICATION
   CONNECTION LIMIT -1
   PASSWORD 'b62Zx8v6-fea7-4a86-Kd33-107d0865593F';

-- ********* CREACION DE LA BD *************
CREATE DATABASE beware_software
   with owner=beware_software
   encoding='UTF8'
   tablespace=pg_default
   CONNECTION LIMIT=-1;

-- ********* IMPLEMENTACION DE LA BASE DE DATOS *****************
create table victimizer (
	ci integer not null primary key,
	name text not null,
	profile text not null -- nombre del archivo .jpg .png de la foto del victimizador
);

create table contact (
	cellphone varchar(15) not null primary key,	-- 10 chars para el nro telf + el codigo de pais (ej. +591 u otro)
	email text not null,
	name text not null
);

create table "user"(
	ci integer not null primary key,
	name text not null,
	cellphone varchar(15) not null,
	email text not null,
	password text not null,
	state boolean not null,
	type_user boolean not null	-- discriinador de usuario (posible victima [true] o policia [false]) 
);

create table police(
	ci_police integer not null primary key,
	ladder_number varchar(15) not null,
	grade varchar(50) not null,
	foreign key (ci_police) references "user"(ci)
	on update cascade
	on delete cascade
);

create table possible_victim(
	ci_victim integer not null primary key,
	nickname text not null,
	sex boolean not null,			--true: Female, false: Male
	birthday date not null,
	civil_status char(3) not null,	--SOL=soltero, CAS=casado, DIV=dicorciado, VIU=viudo, SEP=separado
	address text not null,
	profile text not null,
	foreign key (ci_victim) references "user"(ci)
	on update cascade
	on delete cascade
);

create table trusted_person(
	ci_victim integer not null,
	ci_person_trusted integer not null,
	primary key (ci_victim, ci_person_trusted),
	foreign key (ci_victim) references possible_victim(ci_victim)
	on update cascade
	on delete cascade,
	foreign key (ci_person_trusted) references possible_victim(ci_victim)
	on update cascade
	on delete cascade
);

create table trusted_contact(
	ci_victim integer not null,
	cellphone_trusted_contact varchar(15) not null,
	primary key (ci_victim, cellphone_trusted_contact),
	foreign key (ci_victim) references possible_victim(ci_victim)
	on update cascade
	on delete cascade,
	foreign key (cellphone_trusted_contact) references contact(cellphone)
	on update cascade
	on delete cascade
);

create table help_request(
	code serial not null primary key,
	date_time timestamp not null,	-- fehca y hora del pedido de auxilio
	initial_location text not null,
	filename1 text not null,
	filename2 text not null,
	ci_victim integer not null,
	ci_victimizer integer null,		-- puede que no se haya identificado al victimizador (null)
	foreign key (ci_victim) references possible_victim(ci_victim)
	on update cascade
	on delete cascade,
	foreign key (ci_victimizer) references victimizer(ci)
	on update cascade
	on delete cascade
);