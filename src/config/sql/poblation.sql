-- ***************************** POBLACION INICIAL DE LA BASE DE DATOS ********************************
insert into "user" (ci, "name", cellphone, email, "password", state, type_user) values(9719837, 'Zuleny Cruz', '+59176627871', 'zuleny.cr@gmail.com', '$2a$12$TNFe/KoFUKhBkAaGs/ML5.QIiW7FJEgwAi3.fNQiR4h3WyltfQkE.', true, true);
insert into "user" (ci, "name", cellphone, email, "password", state, type_user) values(9719836, 'María Rojas', '+59167784048', 'zulenyqq@gmail.com', '$2a$12$TNFe/KoFUKhBkAaGs/ML5.QIiW7FJEgwAi3.fNQiR4h3WyltfQkE.', true, true);
insert into "user" (ci, "name", cellphone, email, "password", state, type_user) values(12345678, 'Juana Perez', '+59176627872', 'juana@gmail.com', '$2a$12$TNFe/KoFUKhBkAaGs/ML5.QIiW7FJEgwAi3.fNQiR4h3WyltfQkE.', true, true);
insert into "user" (ci, "name", cellphone, email, "password", state, type_user) values(6868161, 'Ruddy Quispe', '+59165944030', 'ruddyq18@gmail.com', '$2a$12$TNFe/KoFUKhBkAaGs/ML5.QIiW7FJEgwAi3.fNQiR4h3WyltfQkE.', true, true);
insert into "user" (ci, "name", cellphone, email, "password", state, type_user) values(6868162, 'Juan Perez', '+59173979910', 'uyunicode@gmail.com', '$2a$12$TNFe/KoFUKhBkAaGs/ML5.QIiW7FJEgwAi3.fNQiR4h3WyltfQkE.', true, false);

insert into police (ci_police, ladder_number, grade) values(6868162, 'CE-1245', 'Teniente');

insert into possible_victim (ci_victim, nickname, sex, birthday, civil_status, address, profile) values(9719837, 'zuleny', true, '1997/09/09', 'SOL', 'Avenida Siempre Vivas', 'a.jpg');
insert into possible_victim (ci_victim, nickname, sex, birthday, civil_status, address, profile) values(9719836, 'Maria', true, '2021/03/12', 'SOL', 'avenida landivar', 'profile.jpg');
insert into possible_victim (ci_victim, nickname, sex, birthday, civil_status, address, profile) values(12345678, 'Juana', true, '1998/03/12', 'SOL', 'avenida Banzer', 'profile.jpg');
insert into possible_victim (ci_victim, nickname, sex, birthday, civil_status, address, profile) values(6868161, 'ruddy', false, '1997/11/30', 'SOL', 'avenida Banzer', 'profile.jpg');

insert into victimizer (ci, "name", profile) values(9719831, 'Pedro Valdez', 'a.jpg');
insert into victimizer (ci, "name", profile) values(9719832, 'Ramon Rojas Chambi', 'a.jpg');
insert into victimizer (ci, "name", profile) values(9719833, 'Cristian Choque', 'a.jpg');

insert into help_request (date_time, initial_location, filename1, filename2, ci_victim, ci_victimizer) values('2021/07/23', '-17.687513, -63.149063', 'a.jpg', 'b.jpg', 9719837, 9719831);
insert into help_request (date_time, initial_location, filename1, filename2, ci_victim, ci_victimizer) values('2021/07/23', '-17.687513, -63.149063', 'a.jpg', 'b.jpg', 9719836, 9719832);

insert into contact (cellphone, email, "name") values('+59173610652', 'cruz.rodriguez.zuleny@gmail.com', 'Rebeca Cruz');
insert into contact (cellphone, email, "name") values('+59167784048', 'faquelia.cr.2016@gmail.com', 'Stephani Heredia');

insert into trusted_contact (ci_victim, cellphone_trusted_contact) values(9719837, '+59167784048');
insert into trusted_contact (ci_victim, cellphone_trusted_contact) values(9719837, '+59176627873');

insert into trusted_person (ci_victim, ci_person_trusted) values(9719837, 9719836);
insert into trusted_person (ci_victim, ci_person_trusted) values(9719837, 12345678);
insert into trusted_person (ci_victim, ci_person_trusted) values(9719836, 9719837);