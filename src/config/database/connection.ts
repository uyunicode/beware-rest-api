/**
 * Import Dependencies
 */
import { Pool, QueryResult } from 'pg'
import { config as dotenv } from 'dotenv';

/**
 * @class Database connection (Singleton)
 */
export class Connection {

    /**
     * Attributes
     */
    private static _instance: Connection;
    private session: Pool;

    /**
     * @constructor
     * @private
     * Initializes the instance to the database with the data from the .env file
     * @catch @throws print "Error in private contructor Connection" if there was a connection error
     */
    private constructor() {
        try {
            dotenv();
            this.session = new Pool({
                host: process.env.DB_HOST,
                user: process.env.DB_USERNAME,
                password: process.env.DB_PASSWORD,
                database: process.env.DB_DATABASE,
                port: Number(process.env.DB_PORT)
            });
            this.session.connect();
            console.log(`connected to database success:  ${process.env.DB_DATABASE}`);
        } catch (error) {
            console.error("Error in private contructor Connection", error);
            throw new Error("Error in Connection to database");
        }
    }

    /**
     * @public
     * Singleton pattern: Returns an instance of the connection to the DB
     * @returns instance of connection to the DB
     */
    public static getInstance(): Connection {
        if (!this._instance) {
            this._instance = new Connection();
        }
        return this._instance;
    }

    /**
     * @public
     * Execute an SQL query to the DB and return its response
     * @param sqlQuery SQL query
     * @returns returns the result of the query
     */
    public async executeQuerySQL(sqlQuery: string): Promise<QueryResult> {
        const result = await this.session.query(sqlQuery);
        return result;
    }
}